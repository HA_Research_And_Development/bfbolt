//
//  main.m
//  AsynTaskExample
//
//  Created by Hoang Anh   on 12/24/15.
//  Copyright © 2015 Hoang Anh . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
